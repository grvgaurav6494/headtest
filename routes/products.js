const express = require("express");
const router = express.Router();
// const winston = require('../config/winston');
// const Joi = require("joi");
const Product =  require("../models/products");
const Fabrication = require("../models/fabrications");
const Paper = require("../models/papers");
const authController = require('../controllers/auth');



router.post("/create-requirement", authController.verifyToken, (req, res) => {
	
	let product = new Product(req.body);
	let fabrication = new Fabrication({jobId: req.body.jobId, title: req.body.title, vendor: req.body.vendor, deliveryDate: req.body.expectedDeliveryDate});
	// console.log(req.body);
	// Paper.findOneAndUpdate({ paper: req.body.paperType }, { $inc: { totalOrder: req.body.plates } }, { new: true })
 //    .then((res) => {
 //    	console.log(res);
 //        console.log("Updated successfully!!");
 //    })
 //    .catch(err => {
 //        res.status(400).json({success:false, message:"Jobs not found"});
 //    });

	// Save product and fabrication details
	product.save()
    .then(() => {
    	fabrication.save();
      	res.status(200).json({success: true, message: 'Job created successfully'});
    })
    .catch(err => {
    	res.status(400).json({success:false, message:err});
    });
});



router.get("/listAllProducts", authController.verifyToken, (req, res) => {
	Product.find().sort({created:-1})
	.then((pro) => {
		let paperType = pro[0].papers.map(paper => {
			return paper.paperType;
		});

		let gsm = pro[0].papers.map(paper => {
			return paper.paperGsm;
		});

		let size = pro[0].papers.map(paper => {
			return paper.paperSize;
		});

		let quantity = pro[0].papers.map(paper => {
			return paper.paperCount;
		});

		Object.assign(pro[0], {key3: "value3"});
		// paperType.toString(), gsm.toString(), size.toString(),  quantity.toString();
		pro[0] = {
			"paperType": paperType.toString(),
			"gsm": gsm.toString(),
			"paperSize": size.toString(),
			"plates": quantity.toString(),
		  "workingStatus": 0,
		  "_id": "5c6949af6a14b82cb8474fde",
		  "jobId": '1550403913720',
		  "title": 'Final Test',
		  "vendor": 'Vodafone',
		  "description": 'ANY THING',
		  "startDate": '2019-02-17T18:30:00.000Z',
		  "expectedDeliveryDate": '2019-02-21T18:30:00.000Z',
		  "rimWeight": '300',
		  "unitPrice": '25',
		  "plateType": 'thermal',
		  "specialPaper": '',
		  "printMode": 'Type B Machine'
		}

		// pro[0] = {...newPaperDetails};

		// delete pro[0].papers;
		// pro[0].name = "ravi";
		// pro[0]["paperType"] = paperType.toString();
		// pro[0]["gsm"] =gsm.toString();
		// pro[0]["paperSize"] =size.toString();
		// pro[0]["plates"] =quantity.toString();
		console.log(pro[0])
		res.status(200).json(pro);
	})
	.catch(err => {
    	res.status(400).json({success:false, message:"Jobs not found"});
	});
})

router.get("/listproduct/:id", authController.verifyToken, (req, res) => {
	// console.log(req.headers);
	let jobId = req.params.id;
	Product.find({jobId})
	.then((product) => {
		res.status(200).json(product[0]);
	})
	.catch(err => {
    	res.status(400).json({success:false, message:"Job Id not found"});
	});
})

router.put("/updateProduct/:id", authController.verifyToken, (req, res) => {
	let id = req.params.id;
	console.log(req.body);
	// let paperCount = req.body.papersToAdd;
	// delete req.body.papersToAdd;
	Product.findOneAndUpdate({_id:id}, req.body)
	.then(() => {
		res.status(200).json({success:true, message:"Product updated"});
		// Paper.findOneAndUpdate({ paper: req.body.paperType }, { $inc: { totalOrder: paperCount } }, { new: true })
		// .then(() => {
		// 	res.status(200).json({success:true, message:"Product updated"});

		// })
		// .catch(err => {
		// 	console.log(err)
		// });
	})
	.catch(err => {
    	res.status(400).json({success:false, message:"Job Id not found"});
	});
})

router.post("/remove/:id", authController.verifyToken, (req, res) => {
    Product.findOneAndRemove({jobId: req.params.id}, function(err, product){
    if(err) res.json(err);
    	else {
    		Fabrication.findOneAndRemove({jobId: req.params.id}, function(err, product) {
    			if(err) res.json(err);
    			else res.json('Successfully removed');

    		});
    	}
    });

    Paper.findOneAndUpdate({ paper: req.body.paper }, { $inc: { totalOrder: -req.body.count } }, { new: true })
    .then(() => {
        console.log("Updated successfully!!");
    })
    .catch(err => {
        res.status(400).json({success:false, message:"Jobs not found"});
    });

})


module.exports = router;